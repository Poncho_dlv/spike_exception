#!/usr/bin/env python
# -*- coding: utf-8 -*-

from discord.ext import commands
from spike_translation import Translator


class SpikeError(Exception):
    """Base class for exceptions in spike."""

    def __init__(self):
        # /* Unknown Error */
        self.message = Translator.tr("#_exception.unknown_error")

    def __repr__(self):
        return self.message


class DefaultError(SpikeError):
    def __init__(self, message):
        self.message = message


class CoachNotFoundError(SpikeError):
    def __init__(self, lang, coach_name, platform=None):
        if platform is None:
            # /* Coach **{}** not found */
            self.message = Translator.tr("#_exception.coach_not_found", lang).format(coach_name)
        else:
            # /* Coach **{}** not found on **{}** */
            self.message = Translator.tr("#_exception.coach_not_found_platform", lang).format(coach_name, platform)


class CompetitionNotFoundError(SpikeError):
    def __init__(self, lang, competition_name, league_name=None, platform=None):
        if platform is None and league_name is None:
            # /* Competition **{}** not found */
            self.message = Translator.tr("#_exception.competition_not_found", lang).format(competition_name)
        elif league_name is None and platform is not None:
            # /* Competition **{}** not found on **{}** */
            self.message = Translator.tr("#_exception.competition_not_found_platform", lang).format(competition_name, platform)
        else:
            # /* Competition **{competition_name}** not found in **{league_name}** - **{platform}** */
            self.message = Translator.tr("#_exception.competition_not_found_platform_league", lang).format(competition_name=competition_name, league_name=league_name, platform=platform)


class LeagueNotFoundError(SpikeError):
    def __init__(self, lang, league_name, platform=None):
        if platform is None:
            # /* League **{}** not found */
            self.message = Translator.tr("#_exception.league_not_found", lang).format(league_name)
        else:
            # /* League **{league_name}** not found on **{platform}** */
            self.message = Translator.tr("#_exception.league_not_found_platform", lang).format(league_name=league_name, platform=platform)


class TeamNotFoundError(SpikeError):
    def __init__(self, lang, team, platform=None):
        if platform is None:
            # /* Team **{}** not found */
            self.message = Translator.tr("#_exception.team_not_found", lang).format(team)
        else:
            # /* Team **{}** not found on **{}** */
            self.message = Translator.tr("#_exception.team_not_found_platform", lang).format(team, platform)


class PlayerNotFoundError(SpikeError):
    def __init__(self, lang, player, team=None, platform=None):
        if team is None and platform is None:
            # /* Player **{}** not found */
            self.message = Translator.tr("#_exception.player_not_found", lang).format(player)
        elif team is not None and platform is None:
            # /* Player **{}** not found in team **{}** */
            self.message = Translator.tr("#_exception.player_not_found_team", lang).format(player, team)
        else:
            # /* Player **{player}** not found in team **{team}**  **{platform}** */
            self.message = Translator.tr("#_exception.player_not_found_team_platform", lang).format(player=player, team=team, platform=platform)


class InvalidRaceError(SpikeError):
    def __init__(self, lang, race_label):
        # /* Invalid race: **{race_label}** */
        self.message = Translator.tr("#_exception.invalid_race", lang).format(race_label=race_label)


class InvalidPlatformError(SpikeError):
    def __init__(self, lang, platform):
        # /* Invalid platform: **{platform}** */
        self.message = Translator.tr("#_exception.invalid_platform", lang).format(platform=platform)


class InvalidChannelError(SpikeError):
    def __init__(self, lang, platform=None, label=None):
        if platform is not None:
            # /* Invalid channel: **{}** */
            self.message = Translator.tr("#_exception.invalid_channel", lang).format(platform)
        elif label is not None:
            # /* Invalid channel for **{}** */
            self.message = Translator.tr("#_exception.invalid_channel_label", lang).format(label)
        else:
            # /* Invalid channel */
            self.message = Translator.tr("#_exception.invalid_channel", lang)


class InvalidSyntaxError(SpikeError):
    def __init__(self, lang, message):
        # /* Invalid format:\n{} */
        self.message = Translator.tr("#_exception.invalid_syntax", lang).format(message)


class InvalidEmojiError(SpikeError):
    def __init__(self, lang):
        # /* Invalid emoji */
        self.message = Translator.tr("#_exception.invalid_emoji", lang)


class InvalidRightError(commands.CommandError):
    def __init__(self, lang):
        # /* Invalid permission to use this command */
        self.message = Translator.tr("#_exception.invalid_permission", lang)


class NoPrivateChannelError(commands.CommandError):
    def __init__(self, lang):
        # /* Command unavailable in private message */
        self.message = Translator.tr("#_exception.unavailable_private_message", lang)


class ContestNotFound(SpikeError):
    def __init__(self, lang, coach_name=None):
        if coach_name is None:
            # /* No match found */
            self.message = Translator.tr("#_exception.no_contest_found", lang)
        else:
            # /* No contest found for **{}** */
            self.message = Translator.tr("#_exception.no_contest_found_coach", lang).format(coach_name)
