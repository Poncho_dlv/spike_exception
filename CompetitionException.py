#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_exception import SpikeError
from spike_translation import Translator


class NoCompetitionRegisteredError(SpikeError):
    def __init__(self, lang):
        # /* No competition registered */
        self.message = Translator.tr("#_exception.no_competition_registered", lang)


class CompetitionRegistrationError(SpikeError):
    def __init__(self, lang):
        # /* Error during competition registration */
        self.message = Translator.tr("#_exception.competition_registration_error", lang)
