#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_exception import SpikeError
from spike_translation import Translator


class NoMatchFoundError(SpikeError):
    def __init__(self, lang, coach1_name, coach2_name=None):
        if coach2_name is not None:
            # /* No game found between **{}** and **{}** */
            self.message = Translator.tr("#_statistics_exception.no_games_found_2_coach", lang).format(coach1_name, coach2_name)
        else:
            # /* No matches found for **{}** */
            self.message = Translator.tr("#_statistics_exception.no_games_found_1_coach", lang).format(coach1_name)

